#define padTime     5               /* seconds of padding at begin and end of group for files that need padding */
#define chunkTime   120             /* seconds of data per group */

typedef struct machineState
{
    int nextState;
    int writeLevelFileA;              /* 0 for none, 1 for just padded files, 2 for All */
    int writeLevelFileB;              /* 0 for none, 1 for just padded files, 2 for All */
    struct timeval duration;          /* Number of seconds, microseconds in this state */
    void (*transitOutFunction)(void); /* Function to call at end of state */
} machineState;

static machineState machineStates[] =
{   /* openGroup0 needs to be called before entering first state */
    { 1, 1, 0, {padTime, 0},               NULL},
    { 2, 2, 0, {chunkTime - padTime, 0},   openGroup1},
    { 3, 2, 1, {padTime, 0},               NULL},
    { 4, 1, 2, {padTime, 0},               closeGroup0andMove1to0},
    { 2, 2, 0, {chunkTime - 2*padTime, 0}, openGroup1}
};

static int
process(void)
{
    int i, maxFD=0, currentState=0, localSocket=0;
    struct timeval endStateTime;
    fd_set portFDs;
    FD_ZERO(&portFDs);
    /*  Loop to create the sockets and bind to the ports */
    for (i=0; Apids[i].port > 0; i++)
    {
        struct sockaddr_in poc; /* POC (our) socket */
        Apids[i].fd = socket(PF_INET, SOCK_DGRAM, 0);
        if (Apids[i].fd < 0)
            WriteLogAndDie("**Unable to create socket: %s", strerror(errno));
        if (maxFD < Apids[i].fd)
            maxFD = Apids[i].fd;  /* Keeping the max value of FD for the select statement */
        if (DEBUGGING)
            WriteLog("..Socket created");
        /*
        Under Linux, select() may report a socket file descriptor as "ready for reading", while  nevertheless
        a  subsequent  read blocks.  This could for example happen when data has arrived but upon examination
        has wrong checksum and is discarded.  There may be other circumstances in which a file descriptor  is
        spuriously  reported  as  ready.   Thus  it may be safer to use O_NONBLOCK on sockets that should not
        block.

        So, we set it to non blocking and make sure the recvfrom below returns > 0.
        */
        if (fcntl(Apids[i].fd, F_SETFL, O_NONBLOCK) < 0)
            WriteLogAndDie("**Could not set non-blocking");
        memset((char *) &poc, 0, sizeof(poc));
        poc.sin_family = AF_INET;
        poc.sin_port = htons(Apids[i].port);
        if (Apids[i].port == LIS_STATUS_PORT)
        {
            poc.sin_addr.s_addr = htonl(INADDR_LOOPBACK); /* If this is the status port, only bind to localhost */
            localSocket = Apids[i].fd;
        }
        else
        {
            poc.sin_addr.s_addr = htonl(INADDR_ANY);
        }
        if (bind(Apids[i].fd, (struct sockaddr *) &poc, sizeof(poc)) < 0)
            WriteLogAndDie("**Unable to bind to %d: %s", Apids[i].port, strerror(errno));
        WriteLog("..Bound to port %d", Apids[i].port);
        FD_SET(Apids[i].fd, &portFDs);
    }
    /* Bound to ports now, and fifos are filling.  Set the endStateTime */
    gettimeofday(&currentTime, NULL);
    timeradd(&currentTime, &(machineStates[currentState].duration), &endStateTime);
    openGroup0(); /* Our first state requires that group A be open */
    while (1)
    {   /* Read sockets forever */
        while (!gettimeofday(&currentTime, NULL) && timercmp(&currentTime, &endStateTime, <))
        {   /* While there is time left in this state */
            fd_set rfds   = portFDs;
            struct timeval timeLeftInState;
            int    retval;
            timersub(&endStateTime, &currentTime, &timeLeftInState);
            retval = select(maxFD+1, &rfds, NULL, NULL, &timeLeftInState);
            if (retval == -1)
            {
                WriteLogAndDie("**Unable to select(): %s", strerror(errno));
            }
            else if (retval) /* There is activity on at least one port */
            {
                for (i=0; Apids[i].port > 0; i++)
                {   /* Loop over the ports to see which one has data */
                    if (FD_ISSET(Apids[i].fd, &rfds))
                    {   /* Is it this one? */
                        unsigned char packetBuf[65536];     /* Where to put the incoming packet */
                        struct sockaddr_in sender;          /* Where this packet came from */
                        socklen_t sendsize = sizeof sender;
                        /*  Read in the whole packet  */
                        int length =
                            recvfrom(Apids[i].fd, packetBuf, sizeof(packetBuf), 0,
                                    (struct sockaddr*)&sender, &sendsize);
                        if (length > 0)
                        {   /* Was select() telling the truth? */
                            int readAPID;
                            if (Apids[i].port == LIS_STATUS_PORT)
                            {   /* This is where we will respond with H&S */
                                if (DEBUGGING)
                                    WriteLog("..Status request from %s", inet_ntoa(sender.sin_addr));
                                sendto(Apids[i].fd, "H&S\n", 5, MSG_DONTWAIT,
                                       (struct sockaddr*)&sender, sendsize);
                            }
                            else if ((readAPID = getAPIDofPacket(packetBuf, length)) > 0)
                            {
                                if (readAPID == Apids[i].apid)
                                {
                                    int wlfa = machineStates[currentState].writeLevelFileA;
                                    int wlfb = machineStates[currentState].writeLevelFileB;
                                    /* write level 2 means everything, 1 means just the first apid */
                                    if (wlfa == 2 || (wlfa == 1 && i == 0)) /* i == 0 means the first Apid */
                                    {
                                        fwrite(packetBuf, length, 1, groupInfo[0].filePtrs[i]);
                                    }
                                    if (wlfb == 2 || (wlfb == 1 && i == 0))
                                    {
                                        fwrite(packetBuf, length, 1, groupInfo[1].filePtrs[i]);
                                    }
                                    if (DEBUGGING)
                                        WriteLog("..Packet from %s on port %d",
                                                inet_ntoa(sender.sin_addr), Apids[i].port);
                                    if (relayToTestAndDev && localSocket)
                                    {
                                        int r;
                                        /* Don't need sender anymore in this iteration, so reuse it */
                                        sender.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
                                        sender.sin_port = htons(Apids[i].port + 1);
                                        r = sendto(localSocket, packetBuf, length, MSG_DONTWAIT,
                                                (struct sockaddr*)&sender, sendsize);
                                        if (r < 0)
                                            WriteLog("..Packet repeat to localhost failed: %d -> %s", errno, strerror(errno));
                                        sender.sin_port = htons(Apids[i].port + 2);
                                        r = sendto(localSocket, packetBuf, length, MSG_DONTWAIT,
                                                (struct sockaddr*)&sender, sendsize);
                                        if (r < 0)
                                            WriteLog("..Packet repeat to localhost failed: %d -> %s", errno, strerror(errno));
                                    }
                                }
                                else
                                {
                                    WriteLog("..Packet from %s on port %d was APID %d instead of %d.  Ignoring",
                                            inet_ntoa(sender.sin_addr), Apids[i].port, readAPID, Apids[i].apid);
                                }
                            }
                            else if (readAPID < 0)
                            {
                                WriteLog("..Malformed Packet from %s on port %d",
                                        inet_ntoa(sender.sin_addr), Apids[i].port);
                            }
                        }   /* If there really was data there */
                    }   /* If activity on this port */
                }   /* End loop over ports */
            }   /* If any ports had activity */
        }   /* End of time in current state */
        /* Just in case currentTime passed endStateTime, we want to snap back and use endStateTime */
        /* so filenames line up on the same second */
        currentTime = endStateTime;
        /* Call the transition out function if it exists.  This may open new files */
        if (machineStates[currentState].transitOutFunction)
            machineStates[currentState].transitOutFunction();
        /* Advance to the next state */
        currentState = machineStates[currentState].nextState;
        /* Set the end time of the next state */
        timeradd(&endStateTime, &(machineStates[currentState].duration), &endStateTime);
    }   /* Forever's over */
    return 0;
}
